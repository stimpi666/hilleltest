import java.io.*;
import java.util.*;

public class Task_1 {

    public static void main(String[] args) {

        ArrayList<String> wordList = new ArrayList<>() {};
        boolean value = true;

        try {
            Scanner scanner = new Scanner(new File("D:\\Java\\Hillel_Test\\Hillel_test_text.txt"));
            while (scanner.hasNext()) {
                String word = scanner.next().replaceAll("[-,./;:()]", "");

                for(String words : wordList){
                    value = true;
                    if(word.equalsIgnoreCase(words) || word.equals("")) {
                        value = false;
                        break;
                    }
                }
                if(value) wordList.add(word);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for (String words : wordList) {
            System.out.println(words);
        }
    }
}
