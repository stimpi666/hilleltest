import java.util.Arrays;
import java.util.Collection;

public class Task_2 {

    public <T> void arrayToCollection(T[] array, Collection<T> collection){

        collection.addAll(Arrays.asList(array));
    }
}
