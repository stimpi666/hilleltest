import java.util.concurrent.atomic.AtomicInteger;

public class IncrementSynchronize extends Thread {

    private int value = 0;
    private volatile int volatileValue;
    private AtomicInteger atomicInteger = new AtomicInteger(0);

    public synchronized int getNextValueFirst() {
        return value++;
    }

    public int getNextValueSecond() {
        value = volatileValue++;
        return value;
    }
    public int getNextValueThird(){
        value = atomicInteger.incrementAndGet();
        return value;
    }

    @Override
    public void run() {
        while (value < 100) {
//            System.out.println(Thread.currentThread().getName() + ", value = " + getNextValueFirst());
//            System.out.println(Thread.currentThread().getName() + ", value = " + getNextValueSecond());
            System.out.println(Thread.currentThread().getName() + ", value = " + getNextValueThird());


        }
    }

    public static void main(String[] args) {

        IncrementSynchronize incrementSynchronize = new IncrementSynchronize();

        Thread threadFirst = new Thread(incrementSynchronize);
        threadFirst.setName("threadFirst");
        Thread threadSecond = new Thread(incrementSynchronize);
        threadSecond.setName("threadSecond");
        Thread threadThird = new Thread(incrementSynchronize);
        threadThird.setName("threadThird");

        threadFirst.start();
        threadSecond.start();
        threadThird.start();

    }
}
